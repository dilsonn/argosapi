## About

This is my Argos Graduate developer task submission. The application has been written in the Lumen framework. The application provides two endpoints, the first endpoint gets the top 10 hottest Argos deals from the HotUkDeals API, compares the prices of those deals to Amazon if available and returns the data as a JSON response arranged by highest price difference first. The other endpoint presents this data in a fully responsive web page.


## How to start up the project
1. Download repository
2. Open up a Terminal window
3. Change directory into the project folder
4. Execute the following command `php -S localhost:8000 -t public/`
5. Navigate to localhost:8000 in your browser


## Usage
Once the PHP server is running there are two available routes. The first route is the JSON API endpoint, this is accessed by entering `localhost:8000/api` in your browser. 

The second route is the page which displays the products with the data retrieved from the JSON API, this is accessed by entering `localhost:8000/` in your browser.

## Notes
The API takes around 20 seconds to load, the reason for this is that the Amazon API limits requests to 1 per second. To get around this limit, the API has to sleep after each request to prevent 503 errors.

The price comparisons aren't always 100% accurate as the HUKD API does not provide a unique product idenitifer and getting this information from the Argos website would only slow down the API further.