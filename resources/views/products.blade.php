<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Top 10 Argos Deals</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet">
    <link rel="stylesheet" href='css/style.css'>
</head>
<body>
<header class="header">
    <a href="#"><h1>Top 10 Argos Deals</h1></a>
    <div class="header-form clearfix"></div>
</header>
<div class="container">
    <main class="content">
    @foreach($products as $product)
        <div class="deal clearfix">
            <div class="deal-content">
                <a href="{{ $product->deal_link }}" target="_blank"><h2>{{ $product->title }}</h2></a>
                <br>
                <p class="price">Price - £{{ $product->price }}</p>
                <p class="heat">Deal heat - {{ $product->temperature }}&deg;</p>
                <p class="difference">
                   
                </p>
                <br>
                <h3>Deal Description</h3>
                <p>{{ $product->description }}</p>
            </div>
            <img src="{{ $product->deal_image }}" alt="Bancroft Park Placeholder" class="deal-image">
            <a href="{{ $product->product_url }}" target="_blank"><div class="product-link">Get Deal</div></a>
        </div>
    @endforeach
    </main>
</div>
</body>
</html>
