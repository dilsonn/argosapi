<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use GuzzleHttp\Client as Guzzle;
use ApaiIO\Configuration\GenericConfiguration;
use ApaiIO\Operations\Search;
use ApaiIO\ApaiIO;
use Amazon;


class Controller extends BaseController
{
    /**
     * @var Guzzle
     */
    protected $guzzle;

    /**
     * @var string
     */
    protected $hukdKey = "681de02bc1e9658ea2623d38e34722f3";

    /**
     * @param Guzzle $guzzle
     */
    public function __construct(Guzzle $guzzle)
    {
        $this->guzzle = $guzzle;
    }

    /**
     * Get data from HUKD and process
     * @return json array
     */
    public function getData(){
        //Make HUKD Request
        $response = $this->guzzle->get("http://api.hotukdeals.com/rest_api/v2/?results_per_page=10&key=" . $this->hukdKey ."&merchant=argos");
        $xml = new \SimpleXMLElement($response->getBody());


        $products = array();

        foreach($xml->deals->api_item as $entry){

            if(preg_match("/(?:now)(?:\\s+)(?:�)(\\d+)(\\.)(\\d+)/", $entry->title)){
                preg_match("/(?:now)(?:\\s+)(?:�)(\\d+)(\\.)(\\d+)/", $entry->title,$matches);
                $price = str_replace("now �","",$matches[0]);
            }
            //Use Regex to extract price from title if in pounds
            elseif(preg_match("/\\p{Sc}-?\\d+(?:,\\d{3})*(?:\\.\\d+)?/", $entry->title)){
                preg_match("/\\p{Sc}-?\\d+(?:,\\d{3})*(?:\\.\\d+)?/", $entry->title, $matches);
                $price = substr($matches[0],1);

            }

            //Else price is in pence
            elseif(preg_match("/([0-9]+)(?:p)/", $entry->title, $matches)){
                preg_match("/([0-9]+)(?:p)/", $entry->title, $matches);
                $price =  "0." . substr($matches[0],0,-1);
            }
            else {
                $price = 0;
            }

            //Create link to product page
            $productUrl = "http://www.hotukdeals.com/visit?m=5&q=" . substr($entry->deal_image, strrpos($entry->deal_image, '/') + 1);

            $title = strip_tags($entry->title->asXML());
            $dealLink = strip_tags($entry->deal_link->asXml());
            $dealImage = strip_tags($entry->deal_image->asXml());
            $description = strip_tags($entry->description->asXml());
            $temperature = strip_tags($entry->temperature->asXml());;
            $timestamp = strip_tags($entry->timestamp->asXml());



            
            //Add product details to array
            $product = array(
                "title" => $title,
                "deal_link" => $dealLink,
                "price" => $price,
                "product_url" => $productUrl,
                "deal_image" => $dealImage,
                "description" => $description,
                "temperature" => round($temperature),
                "timestamp" => $timestamp
            );

            //Add product to array
            array_push($products, $product);
        }

      
        return json_encode($products);

    }


    /**
     * Displays the data returned from the API
     * @return mixed
     */
    public function showData(){
        $products = json_decode($this->getData());
        return view('products')->withProducts($products);
    }

}

